# PySched #

This is an experiment in whether we can neatly encapsulate Parallel Task Graph (PTG) information in a neat Python form.
Rather than defining things in terms of tasks (as in PARSEC) we define things in terms of data and infer tasks from these tasks, allowing a formulation closer to the mathematics.

NOTE: This is a toy project, it will probably not be developed into a full product.

### How do I get set up? ###

* Have python3.
* Make it up as you go along.