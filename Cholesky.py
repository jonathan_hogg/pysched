#!/usr/bin/python3

import pysched.base
import pysched.jdf
import pysched.method
import pysched.reduce
import pysched.tags
import pysched.types

@pysched.method.wrap(overwrite="A")
def BlockCholesky(A):
    info = dpotrf('L', A.n, A.n, A.data, A.lda)
    if info != 0:
        raise NotPosdefException(A.first_row_idx + info)

@pysched.method.wrap(overwrite="A_ij")
def BlockApply(A_ij, L_jj):
    dtrsm('L', 'T', A_ij.m, A_ij.n, 1.0, A_ij.data, A_ij.lda, L_jj.data,
        L_jj.lda)

@pysched.method.wrap(overwrite="Ak")
def BlockUpdate(Ak, L_ik, L_jk):
    if L_ik is L_jk:
        dsyrk()
    else:
        dgemm()

class Cholesky(pysched.base.Algorithm):
    n = pysched.base.Dimension()
    A = pysched.types.DenseMatrix(n,n,tags=pysched.tags.IN)
    Ak = pysched.types.DenseMatrix(n,n,alias=A)
    L = pysched.types.DenseMatrix(n,n,tags=pysched.tags.OUT,alias=A)
    i = pysched.base.Index()
    j = pysched.base.Index()
    k = pysched.base.Index()
    L[i,i] = BlockCholesky(Ak[i,i])
    L[i,j] = BlockApply(Ak[i,j], L[i,i])    | (i > j)
    Ak[i,j] = A[i,j] + pysched.reduce.sum(BlockUpdate(L[i,k], L[j,k]), (0<k<j))

pysched.jdf.print_jdf(Cholesky())
