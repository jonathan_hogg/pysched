import pysched.base

class DenseMatrix(pysched.base.SchedObject):
    """A scheduling object backed by a dense matrix"""
    def __init__(self, m, n, tags=None, alias=None):
        super().__init__(tags)
        self.alias = alias
        self.m = m
        self.n = n
        if alias is not None:
            if not isinstance(alias, DenseMatrix):
                raise TypeError("Can only alias DenseMatrix to DenseMatrix")
            if alias.m != self.m or alias.n != self.n:
                raise TypeError("DenseMatrix alias dimensions must match")

    def get_base_range_(self, idx):
        """Return range given index position (i.e. 0 or 1),
           don't take constraints into account"""
        if idx<0 or idx>1: raise IndexError("DenseMatrix must have two indices")
        if idx==0:
            return (0, self.m-1)
        else:
            return (0, self.n-1)

    def get_range(self, idx, idx_expr):
        """Return range of given idx in idx_expr"""
        base_range = self.get_base_range_(idx_expr.get_posn(idx))
        return idx_expr.modify_range(base_range, idx)

    def print_jdf_defn(self, out):
        if self.alias: return # Noop, defined by alias
        out.write(self.name+'   [type="struct tiled_matrix_desc_t*"]\n')

class DenseMatrixInstance:
    """Actual block type passed into routines."""

    def __init__(self, abstract, m, n, resource):
        self.abstract = abstract # Abstract representation this is instance of
