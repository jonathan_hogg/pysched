import itertools
import sys

from pysched.base import *
from pysched.task import *

def print_jdf(alg, outfile=sys.stdout):
    # Output file
    print_jdf_preamble(alg.vars, outfile)
    for t in alg.tasks:
        t.jdf_output(outfile)

def print_jdf_preamble(vars, outfile=sys.stdout):
    """Print preamble for JDF file"""
    outfile.write('extern "C" %{\n')
    outfile.write('#include "dague.h"\n')
    outfile.write('#include "data_distribution.h"\n')
    outfile.write('%}\n\n')
    for idx in vars:
        idx.print_jdf_defn(outfile)
