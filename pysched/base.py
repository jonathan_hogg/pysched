import collections
import itertools

class VarMixin:
    """Base type for anything that needs defined as a variable by user"""
    def __init__(self):
        self.name = "Unnamed Variable"
    def set_name(self, name):
        self.name = name

class Dimension(VarMixin):
    """Represents a dimension of a SchedObject"""
    def print_jdf_defn(self, out):
        out.write(self.name + "   [type = int]\n")
    def __add__(self, other):
        return SimpleExpr(self, '+', other)
    def __sub__(self, other):
        return SimpleExpr(self, '-', other)
    def __repr__(self):
        return self.name

class SimpleExpr:
    """Represents an expression involving Dimensions/Indices and constants"""
    def __init__(self, lhs, op, rhs):
        self.lhs = lhs
        self.op = op
        self.rhs = rhs
    def __repr__(self):
        return str(self.lhs)+self.op+str(self.rhs)

class Index:
    """Represents an algebraic index of a SchedObject"""
    _counter = itertools.count() # used to name indices
    def __init__(self, name=None):
        self.index = next(Index._counter)
        self.name = name
    def set_default_name(self, name):
        """Sets variable name if there is no name already"""
        if not self.name: self.name = name
    def __repr__(self):
        if self.name: return self.name
        return "Index"+str(self.index)
    def __lt__(self, other):
        if isinstance(other, int):
            return Relation(self, "<", other)
        if isinstance(other, Index):
            return Relation(self, "<", other)
        raise TypeError("Index not comparable with " + str(type(other)) + ".")
    def __gt__(self, other):
        if isinstance(other, int):
            return Relation(other, "<", self)
        if isinstance(other, Index):
            return Relation(other, "<", self)
        raise TypeError("Index not comparable with " + str(type(other)) + ".")
    def __add__(self, other):
        return SimpleExpr(self, '+', other)
    def __sub__(self, other):
        return SimpleExpr(self, '-', other)
    @property
    def min(self):
        return '0'
    @property
    def max(self):
        return 'SIZE_'+str(self)

class Relation:
    """Combination of Indices defining a restricted range."""
    def __init__(self, lhs, relation, rhs):
        self.lhs = lhs
        self.relation = relation
        self.rhs = rhs
    def __repr__(self):
        return "(" + repr(self.lhs) + self.relation + repr(self.rhs) + ")"
    def __iter__(self):
        """Return iterator so python in operator will work"""
        return iter([self.lhs, self.rhs])
    def modify_range(self, idxrange, idx):
        """Modify range to take account of relation. wlog we assume rhs
           retains full range and lhs is restricted."""
        if idx not in self:
            raise IndexError(str(idx)+' not in '+str(self))
        if idx==self.rhs:
            return idxrange # rhsassumed to have full range
        # otherwise must be lsh
        if self.relation == "<":
            return (idxrange[0], self.rhs-1)
        elif self.relation == ">":
            return (self.rhs+1, idxrange[1])

class SchedRHSMixin:
    """Classes inheriting from this type are valid RHS types for SchedObject
    definitions."""
    def __add__(self, other):
        if not isinstance(other, SchedRHSMixin):
            raise TypeError("Cannot add SchedRHS to non-SchedRHS.")
        return SchedExpression(self, "+", other)

class SchedExpression(SchedRHSMixin):
    """Expression involving multiple objects of type SchedRHSMixin."""
    def __init__(self, lhs, op, rhs):
        if not isinstance(lhs, SchedRHSMixin):
            raise TypeError("lhs not a valid Sched RHS.")
        self.lhs = lhs
        self.op = op
        if not isinstance(rhs, SchedRHSMixin):
            raise TypeError("rhs not a valid Sched RHS.")
        self.rhs = rhs

class IndexTuple(tuple):
    """Represents and index tuple, adding some extra methods."""
    def get_posn(self, idx):
        i = 0
        for x in self:
            if x == idx: return i
            i+=1
        raise IndexError(str(idx)+' does not occur in '+str(self))
    def get_set(self):
        return set(self)
    def modify_range(self, idxrange, idx):
        """This is a noop for a tuple."""
        return idxrange

class SchedObject(VarMixin):
    """Base type of scheduler objects, handles indexing."""
    def __init__(self, tags):
        self.definitions = {}
        self.tags = tags

    def __getitem__ (self, key):
        return IndexedSchedObject(self, key)
   
    def __setitem__(self, key, value):
        import pysched.method
        if not isinstance(value, SchedRHSMixin):
            raise TypeError("Invalid rhs for SchedObject definition.")
        key = IndexTuple(key)
        if isinstance(value, pysched.method.RestrictedWrap):
            ri = RestrictedIndex(key, value.relation)
            self.definitions[ri] = value.wf
        else:
            self.definitions[key] = value

    def __str__(self):
        return str(self.definitions)

    def is_input(self):
       return self.tags.is_input() if self.tags else False
    def is_output(self):
       return self.tags.is_output() if self.tags else False

class RestrictedIndex:
    def __init__(self, idx, restriction):
        self.idx = idx
        self.restriction = restriction
    def __repr__(self):
        return "(" + repr(self.idx) + "|" + repr(self.restriction) + ")"
    def get_set(self):
        return set(self.idx)
    def get_posn(self, idx):
        return self.idx.get_posn(idx)
    def modify_range(self, idxrange, idx):
        if idx in self.restriction:
            return self.restriction.modify_range(idxrange, idx)
        else:
            return idxrange # noop

class IndexedSchedObject(SchedRHSMixin):
    """Combination of SchedObject with Index tuple"""
    def __init__(self, obj, idx):
        self.base = obj
        self.idx = idx
    def __repr__(self):
        return repr(self.base) + "[" + repr(self.idx) + "]"

class OrderedClass(type):
    """Metaclass for preserving order of declarations"""
    @classmethod
    def __prepare__(metacls, name, bases):
        return collections.OrderedDict()
    def __new__(self, name, bases, classdict):
        classdict['__ordered__'] = [key for key in classdict.keys()
                if key not in ('__module__', '__qualname__')]
        return type.__new__(self, name, bases, classdict)

class Algorithm(metaclass=OrderedClass):
    """Base type of Algorithms"""
    def __init__(self):
        from pysched.task import Task
        # Create list of SchedObjects
        self.schedobj = {x: getattr(self, x) for x in self.__ordered__
                if isinstance(getattr(self, x), SchedObject)}
        self.vars = [getattr(self, x) for x in self.__ordered__
                if isinstance(getattr(self, x), VarMixin)]
        # Set names for indices from definitions
        for x in self.__ordered__:
            if isinstance(getattr(self,x), Index):
                getattr(self,x).set_default_name(x)
            if isinstance(getattr(self,x), VarMixin):
                getattr(self,x).set_name(x)

        # Build PTG tasks and descriptions
        self.outputs = [
                obj for soname, obj in self.schedobj.items() if obj.is_output()
                ]
        self.tasks = [] # list of tasks
        to_define = []
        for target in self.outputs:
            for idx, defn in target.definitions.items():
                to_define.append((target, idx))
        while len(to_define) > 0:
            (target, idx) = to_define.pop()
            self.tasks.append(Task(target[idx]))

    def exec(self, *args):
        """Execture the algorithm on supplied data"""
        pass
