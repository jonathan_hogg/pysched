class IN:
    """Tag representing something as an input."""
    @staticmethod
    def is_input(): return True
    @staticmethod
    def is_output(): return False
class OUT:
    """Tag representing something as an output."""
    @staticmethod
    def is_input(): return False
    @staticmethod
    def is_output(): return True
