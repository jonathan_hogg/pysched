import pysched.base
import pysched.method

class Reduction(pysched.base.SchedRHSMixin):
    """Represents a reduction of wf across idx_range."""
    def __init__(self, wf, idx_range):
        if not isinstance(wf, pysched.method.BoundSchedulable):
            raise TypeError("Cannot reduce invalid function.")
        self.wf = wf
        if not isinstance(idx_range, pysched.base.Relation):
            raise TypeError("Cannot reduce across invalid range.")
        self.idx_range = idx_range

class sum(Reduction):
    """Calculates sum of wf output across idx_range."""
    def __init__(self, wf, idx_range):
        super().__init__(wf, idx_range)
