import itertools
import sys

from pysched.base import *

class Task:
    _counter = itertools.count()
    def __init__(self, obj):
        # Set up information about task
        self.obj = obj # IndexedSchedObject
        self.idx_expr = obj.idx # may have repetition and constraints
        self.idx_set = obj.idx.get_set() # set of unique indices involved
        self.name = "Defn" + str(next(self._counter)) + "("
        self.name += ",".join(str(x) for x in self.idx_set)
        self.name += ")"
        self.defn = obj.base.definitions[obj.idx]
        # Figure out inputs/outputs
        self.outputs = [] # will be filled in by dependencies later
        self.inputs = self.defn.get_inputs()

    def jdf_output(self, out):
        # Header comment to make generated code more readable
        out.write('\n')
        out.write("/*********************************************************\n")
        out.write(" * "+self.name+'\n')
        out.write(" ********************************************************/\n")
        # Name of task used by system
        out.write(self.name+'\n\n')
        # What variable ranges are we defined on?
        out.write("// Execution space\n")
        for idx in self.idx_set:
            idx_range = self.obj.base.get_range(idx, self.idx_expr)
            out.write(str(idx)+' = '+str(idx_range[0])+'...'+str(idx_range[1])+'\n')
        # Parallel partitioning ie where is task executed
        out.write("\n// Parallel partitioning\n")
        out.write("FIXME!\n\n")
        # What are our inputs and outputs? How do we communicate with them?
        out.write("// Parameters\n")
